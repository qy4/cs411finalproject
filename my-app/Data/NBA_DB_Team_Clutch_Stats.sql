-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: NBA_DB
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Team_Clutch_Stats`
--

DROP TABLE IF EXISTS `Team_Clutch_Stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team_Clutch_Stats` (
  `TeamName` varchar(255) DEFAULT NULL,
  `Field_Goal_P` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Team_Clutch_Stats`
--

LOCK TABLES `Team_Clutch_Stats` WRITE;
/*!40000 ALTER TABLE `Team_Clutch_Stats` DISABLE KEYS */;
INSERT INTO `Team_Clutch_Stats` VALUES ('DEN',45.4),('PHI',44.8),('LAC',48.2),('MIL',46.8),('SAS',47.3),('POR',42.8),('TOR',43),('HOU',38.8),('GSW',43.5),('IND',49.5),('BOS',50),('BKN',44.3),('SAC',40.8),('LAL',44.8),('ATL',45.7),('OKC',38.2),('DET',44.5),('ORL',40.9),('MIA',39),('CHA',39.7),('UTA',42.9),('DAL',39.5),('MIN',39.9),('MEM',41.6),('CLE',39.4),('CHI',43),('WAS',42.7),('NOP',40.9),('PHX',47.1),('NYK',40.1);
/*!40000 ALTER TABLE `Team_Clutch_Stats` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-04 17:24:04
