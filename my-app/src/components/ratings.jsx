import React, { Component } from "react";
import axios from 'axios';
class Ratings extends Component {
  state = {
    btnDeleteInfo: { ratingId: "", output: "" },
    btnAddInfo: {
      userName: "",
      playerName: "",
      rating: "",
      comment: "",
      output: ""
    }

  };


  selectStyle = {
    marginLeft: "600px",
    position: "absolute",
    top: "270px",
    width: "200px",
    height: "100px",
    fontSize: "20px"
  };



  /* ============Delete Button functions================ */

  handleDeleteClick = () => {
    console.log(
      "BUTTON CLICKED! user wanna delete rating:",
      this.state.btnDeleteInfo.ratingId
    );
    axios.post("http://127.0.0.1:5000/delete_comment/" + this.state.btnDeleteInfo.ratingId, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      alert('Successfully deleted');
    }).catch(function (error) {
      alert(error);
    });
    var btnDeleteInfo_new = this.state.btnDeleteInfo;
    btnDeleteInfo_new.output = this.deleteOutput();
    this.setState({ btnDeleteInfo: btnDeleteInfo_new });
  };

  handleDeleteChange = e => {
    var ratingIdToDelete = e.target.value;
    // console.log("input changes.  pName = ", pName);
    var btnDeleteInfo_new = this.state.btnDeleteInfo;
    btnDeleteInfo_new.ratingId = ratingIdToDelete;
    this.setState({ btnDeleteInfo_new });
  };

  deleteOutput = () => {
    var output =
      "Deleted rating with ratingId: " + this.state.btnDeleteInfo.ratingId;
    return output;
  };

  /* ============Add Rating  Button functions================ */

  handleAddClick = () => {
    console.log("BUTTON CLICKED! user wanna add new rating + comment:");
    axios.post("http://127.0.0.1:5000/add_comment/" + this.state.btnAddInfo.playerName + "/" + this.state.btnAddInfo.userName + "/" + this.state.btnAddInfo.comment + "/" + this.state.btnAddInfo.rating, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      // temp = response.data
      // console.log(temp)
      alert('Result is : ' + response.data);
    }).catch(function (error) {
      alert(error);
    });

    // "Added new rating :" +
    //   this.state.btnAddInfo.rating +
    //   " by user " +
    //   this.state.btnAddInfo.userName +
    //   " for player " +
    //   this.state.btnAddInfo.playerName +
    //   "\nNew Comment: " +
    //   this.state.btnAddInfo.comment;

    var btnAddInfo_new = this.state.btnAddInfo;
    btnAddInfo_new.output = this.addOutput();
    this.setState({ btnAddInfo: btnAddInfo_new });
  };

  handleAddChange_userName = e => {
    var newUserName = e.target.value;
    // console.log("input changes.  pName = ", pName);
    var btnAddInfo_new = this.state.btnAddInfo;
    btnAddInfo_new.userName = newUserName;
    this.setState({ btnAddInfo: btnAddInfo_new });
  };
  handleAddChange_pName = e => {
    var playerName = e.target.value;
    // console.log("input changes.  pName = ", pName);
    var btnAddInfo_new = this.state.btnAddInfo;
    btnAddInfo_new.playerName = playerName;
    this.setState({ btnAddInfo: btnAddInfo_new });
  };
  handleAddChange_rating = e => {
    var rating = e.target.value;
    // console.log("input changes.  pName = ", pName);
    var btnAddInfo_new = this.state.btnAddInfo;
    btnAddInfo_new.rating = rating;
    this.setState({ btnAddInfo: btnAddInfo_new });
  };
  handleAddChange_comment = e => {
    var comment = e.target.value;
    // console.log("input changes.  pName = ", pName);
    var btnAddInfo_new = this.state.btnAddInfo;
    btnAddInfo_new.comment = comment;
    this.setState({ btnAddInfo: btnAddInfo_new });
  };

  addOutput = () => {
    var output =
      "Successfully added new rating: <" +
      this.state.btnAddInfo.rating +
      "> by user: <" +
      this.state.btnAddInfo.userName +
      "> for player: <" +
      this.state.btnAddInfo.playerName +
      "> with comment: <" +
      this.state.btnAddInfo.comment +
      ">";
    return output;
  };


  render() {
    return (
      <div>
        <div style={{ paddingLeft: "100px", fontSize: "20px" }}> {/*start form+btn  div*/}


          {/* ============================Add Rating Button============================  */}
          <br></br>
          <span style={this.styles} className="badge badge-primary m-2">
            Add New Rating
        </span>
          <form>
            <label>
              User Name:
            <input
                type="text"
                name="name"
                onChange={this.handleAddChange_userName}
                placeholder="user full name"
              />
            </label>
            <br></br>
            <label>
              Player Name:
            <input
                type="text"
                name="name"
                onChange={this.handleAddChange_pName}
                placeholder="player full name"
              />
            </label>
            <br></br>
            <label>
              Rating:
            <input
                type="text"
                name="name"
                onChange={this.handleAddChange_rating}
                placeholder="new rating"
              />
            </label>
            <br></br>
            <label>
              Comment:
            <input
                type="text"
                name="name"
                onChange={this.handleAddChange_comment}
                placeholder="new comment"
              />
            </label>
          </form>
          <button
            onClick={this.handleAddClick}
            className="btn btn-info btn-sm m-2"
            style={{ fontSize: "15px" }}
          >
            Add
        </button>
          <h4>{this.state.btnAddInfo.output}</h4>


          {/* ============================Delete Button============================  */}
          <br></br>
          <span style={this.styles} className="badge badge-primary m-2">
            Delete By ratingId:
        </span>
          <form>
            <label>
              Rating Id:
            <input
                type="text"
                name="name"
                onChange={this.handleDeleteChange}
                placeholder="type in rating id"
              />
            </label>
          </form>
          <button
            onClick={this.handleDeleteClick}
            className="btn btn-danger btn-sm m-2"
          >
            Delete
        </button>
          <h4>{this.state.btnDeleteInfo.output}</h4>

        </div> {/*end form+btn div*/}

      </div>

    );

  }


}

export default Ratings;
