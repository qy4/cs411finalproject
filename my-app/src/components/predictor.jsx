import React, { Component } from "react";
import axios from 'axios';

var image1 = require("./advanced_one.png");
// console.log("=====here111");
var image3;
const imgPlaceholder = require("./imgPlaceholder.png");
class Predictor extends Component {
  state = {
    inputedPlayer: "",
    playerOutput: "",

    dropDownMonthList: ["Unselected", 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    selectedMonth: "",
    monthOutput: "",

    dropDownTeamList: ["Unselected",'ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DAL', 'DEN', 'DET', 'GSW', 'HOU', 'IND', 'LAC', 'LAL', 'MEM', 'MIA', 'MIL', 'MIN', 'NOP', 'NYK', 'OKC', 'ORL', 'PHI', 'PHX', 'POR', 'SAC', 'SAS', 'TOR', 'UTA', 'WAS'],
    selectedTeam: "",
    teamOutput: "",

    img1: imgPlaceholder,

  };


  selectStyle = {
    marginLeft: "0px",
    // position: "absolute",
    marginTop: "0px",
    width: "200px",
    height: "100px",
    fontSize: "20px"
  };

  promptStyle = {
    // marginLeft: "0px",
    // position: "absolute",
    marginTop: "50px",
    // width: "200px",
    // height: "100px",
    fontSize: "20px",
    fontWeight: "bold"
  };



  // -------------------------Find ---------------------------------------------

  handleFindChange = e => {
    var inputedPlayer_new = e.target.value;
    this.setState({ inputedPlayer: inputedPlayer_new });
    
  };


  handleFindClick = () => {

    // console.log("http://127.0.0.1:5000/players/" + this.state.btnFindInfo.playerName)
    // axios.get("http://127.0.0.1:5000/players/" + this.state.btnFindInfo.playerName, {
    //   headers: {
    //     'Access-Control-Allow-Origin': '*',
    //   }
    // }).then(function (response) {

    //   alert('Result is : ' + response.data);
    // }).catch(function (error) {
    //   alert(error);
    // });
    var playerOutput_new = "We now find info of player: " + this.state.inputedPlayer;
    this.setState({ playerOutput: playerOutput_new });
  };


  // ---------------------------Drop Down 1: Month-------------------------------------------

  handleSelectMonthChange = e => {
    var selectedMonth_new = e.target.value;
    this.setState({ selectedMonth: selectedMonth_new });
  };

  handleMonthClick = () => {
    var monthOutput_new = "User has selected month: " + this.state.selectedMonth;
    this.setState({ monthOutput: monthOutput_new });

  };


  // ---------------------------Drop Down 2: Opponent Team-------------------------------------------

  handleSelectTeamChange = e => {
    var selectedTeam_new = e.target.value;
    this.setState({ selectedTeam: selectedTeam_new });
  };

  handleTeamClick = () => {
    axios.get("http://127.0.0.1:5000/stat_predictor/"+this.state.inputedPlayer+"/"+this.state.selectedTeam+"/"+ this.state.selectedMonth, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      alert('Result is : ' + response.data);
    }).catch(function (error) {
      alert(error);
    });

    setTimeout(function () {
      this.setState({ img1: image1 });
    
    }.bind(this), 5000);


    // setTimeout(function () {
    //   var n = (this.state.inputedPlayer).substring(0,3);
    //   var s = './advanced_one'+n+this.state.selectedTeam+this.state.selectedMonth+'.png';
    //   console.log(s);
    //   image1 = require(s);
    //   this.setState({ img1: image1 });
    // }.bind(this), 10000);
  };



  handleTeamClick2 = () => {
    // var teamOutput_new = "User has selected month: " + this.state.selectedTeam;
    // this.setState({ teamOutput: teamOutput_new });

    // console.log("=====here1");
    // setTimeout(function () {
    //   image1 = require("./advanced_one.png");
    //   this.setState({ img1: image1 });
    // }.bind(this), 10000);
    // image1 = require("./advanced_oneone.png");
    // image1 = require("./try.jpg");
    // image3 = require("")
    // image1 = require("./advanced_oneone.png");
    // image3 = require("./advanced_one.png");
    
    // console.log("=====here2");
  };
  /* ===========================Render========================== */


  render() {
    return (
      <div style={{ paddingLeft: "100px" }} >
        <div> {/*start form+btn  div*/}
          {/* ===========================Find Button==========================  */}
          <br></br>
          <form>
            <label style={this.promptStyle}>
              1.Enter player name:
            <input
                type="text"
                name="name"
                onChange={this.handleFindChange}
                placeholder="type in full name"
              />
            </label>
          </form>
          {/* <button
            onClick={this.handleFindClick}
            className="btn btn-info btn-sm m-2"
          >
            PlayerFind
        </button> */}
          {/* <p style={{ fontSize: "20px" }}>{this.state.playerOutput}</p> */}

        </div> {/*end form+btn div*/}



        {/* ============================Drop down 1: Month============================  */}
        <h5 style={this.promptStyle}>2. Select Month:</h5>

        <div style={this.selectStyle}> {/*start drowm down div*/}

          <select onChange={this.handleSelectMonthChange} >
            {
              this.state.dropDownMonthList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>
          <br></br>
          {/* <button
            onClick={this.handleMonthClick}
            className="btn btn-info btn-sm m-2"
          >
            MonthSelect
        </button> */}
          {/* <p style={{ fontSize: "20px" }}>{this.state.monthOutput}</p> */}

        </div > {/*end select div*/}


        {/* ============================Drop down 2: Opponent Team============================  */}
        

        <div style={{marginTop:"-80px"}}> {/*start drowm down div*/}
        <h5 style={this.promptStyle}>3. Select Opponent Team:</h5>

          <select onChange={this.handleSelectTeamChange} >
            {
              this.state.dropDownTeamList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>
          <br></br>
          <button
            onClick={this.handleTeamClick}
            className="btn btn-info btn-sm m-2"
          >
            Predict
  </button>
  <br></br>
  {/* <button
            onClick={this.handleTeamClick2}
            className="btn btn-info btn-sm m-2"
          >
            Generate Image
  </button> */}
          <p style={{ fontSize: "20px" }}>{this.state.teamOutput}</p>
          <div>
            <a><img src={this.state.img1} alt="image 1 placeholder" style={{ width: "420px", height: "320px" }} /></a>
         </div>
        </div > {/*end select div*/}




        {/* ============================end Drop downs============================  */}

      </div >

    );

  }


}

export default Predictor;
