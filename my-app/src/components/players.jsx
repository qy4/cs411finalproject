import React, { Component } from "react";
import axios from 'axios';


const image1 = require("./advanced_two_pg1.png");
const image2 = require("./advanced_two_pg2.png");

const imgPlaceholder = require("./imgPlaceholder.png");

class Players extends Component {
  state = {
    inputedPlayer: "",
    playerOutput: "",

    dropDownSeasonList: ["Unselected", "2018-19", "2017-18", "2016-17", "2015-16", "2014-15"],
    selectedSeason: "",
    seasonOutput: "",

    inputedStoryPlayer: "",
    storyPlayerOutput: "",

    img1: imgPlaceholder,
    img2: imgPlaceholder

  };




  selectStyle = {
    marginLeft: "0px",
    // position: "absolute",
    marginTop: "0px",
    width: "200px",
    height: "100px",
    fontSize: "20px"
  };

  promptStyle = {
    // marginLeft: "0px",
    // position: "absolute",
    marginTop: "20px",
    // width: "200px",
    // height: "100px",
    fontSize: "20px",
    fontWeight: "bold"
  };


  handleFindChange = e => {
    var inputedPlayer_new = e.target.value;
    this.setState({ inputedPlayer: inputedPlayer_new });
  };





  handleFindClick = () => {
    var result = "";
    console.log("http://127.0.0.1:5000/players/" + this.state.inputedPlayer)
    axios.get("http://127.0.0.1:5000/players/" + this.state.inputedPlayer, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      result = response.data;

      alert('Result is : ' + response.data);
      // this.handleFindClickHelper(result);
    })



  };

  handleFindClickHelper = (result) => {
    var playerOutput_new = "We now find info of player: " + this.state.inputedPlayer + result;
    console.log("======here");
    this.setState({ playerOutput: playerOutput_new });
  };



  handleSelectSeasonChange = e => {
    var selectedSeason_new = e.target.value;
    this.setState({ selectedSeason: selectedSeason_new });
  };


  handleSeasonClick = () => {
    console.log("http://127.0.0.1:5000/players/" + this.state.inputedPlayer + "/" + this.state.selectedSeason)
    axios.get("http://127.0.0.1:5000/players/" + this.state.inputedPlayer + "/" + this.state.selectedSeason, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      // response_data = response.data;
      alert('Result is : ' + response.data);
    }).catch(function (error) {
      alert(error);
    });
    var seasonOutput_new = "User has selected season: " + this.state.selectedSeason;
    this.setState({ seasonOutput: seasonOutput_new });

  };


  handleStoryFindChange = e => {
    var inputedStoryPlayer_new = e.target.value;
    this.setState({ inputedStoryPlayer: inputedStoryPlayer_new });
  };


  handleStoryFindClick = () => {

    console.log("http://127.0.0.1:5000/ten_best_teammates/" + this.state.inputedStoryPlayer)
    axios.get("http://127.0.0.1:5000/ten_best_teammates/" + this.state.inputedStoryPlayer, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      alert('Result is : ' + response.data);
    }).catch(function (error) {
      alert(error);
    });
    var storyPlayerOutput_new = "Get story of player: " + this.state.inputedStoryPlayer;
    this.setState({ storyPlayerOutput: storyPlayerOutput_new });



    setTimeout(function () {
      this.setState({ img1: image1 });
      this.setState({ img2: image2 });
    }.bind(this), 5000);

    // setTimeout('', 3000);

    // this.setState({ img1: image1 });
    //   this.setState({ img2: image2 });

  };



  /* ===========================Render========================== */


  render() {
    return (
      <div style={{ paddingLeft: "100px" }} >
        <div> {/*start form+btn  div*/}
          {/* ===========================Find Button==========================  */}
          <br></br>
          <form>
            <label style={this.promptStyle}>
              1.Enter player name:
            <input
                type="text"
                name="name"
                onChange={this.handleFindChange}
                placeholder="type in full name"
              />
            </label>
          </form>
          <button
            onClick={this.handleFindClick}
            className="btn btn-info btn-sm m-2"
          >
            PlayerFind
        </button>
          <p style={{ fontSize: "20px" }}>{this.state.playerOutput}</p>

        </div> {/*end form+btn div*/}



        {/* ============================Drop down select============================  */}
        <h5 style={this.promptStyle}>2. Select Season:</h5>

        <div style={this.selectStyle}> {/*start drowm down div*/}

          <select onChange={this.handleSelectSeasonChange} >
            {
              this.state.dropDownSeasonList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>

          <button
            onClick={this.handleSeasonClick}
            className="btn btn-info btn-sm m-2"
          >
            SeasonFind
        </button>
          <p style={{ fontSize: "20px" }}>{this.state.seasonOutput}</p>

        </div > {/*end select div*/}

        {/* ============================3. Get Story============================  */}
        <div>
          <br></br>
          <form>
            <label style={this.promptStyle}>
              3. Advanced function 2: get story of player:
            <input
                type="text"
                name="name"
                onChange={this.handleStoryFindChange}
                placeholder="type in full name"
              />
            </label>
          </form>
          <button
            onClick={this.handleStoryFindClick}
            className="btn btn-info btn-sm m-2"
          >
            PlayerFindStory
        </button>
          <p style={{ fontSize: "20px" }}>{this.state.storyPlayerOutput}</p>
          <div>
            <a><img src={this.state.img1} alt="image 1 placeholder" style={{ width: "420px", height: "320px" }} /></a>
            <a><img src={this.state.img2} alt="image 2 placeholder" style={{ width: "420px", height: "320px", paddingLeft: "20px" }} /></a>
          </div>




        </div>


        {/* ============================end all 3 parts============================  */}

      </div >

    );

  }


}

export default Players;
