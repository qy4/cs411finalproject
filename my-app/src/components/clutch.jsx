import React, { Component } from "react";
import axios from 'axios';
class Clutch extends Component {
  state = {

    dropDownList: ["Unselected", 'ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DAL', 'DEN', 'DET', 'GSW', 'HOU', 'IND', 'LAC', 'LAL', 'MEM', 'MIA', 'MIL', 'MIN', 'NOP', 'NYK', 'OKC', 'ORL', 'PHI', 'PHX', 'POR', 'SAC', 'SAS', 'TOR', 'UTA', 'WAS'],
    selectedDD: "",
    ddOutput: "",

    dropDownList2: ["Unselected", "Regular_Season", "Playoff"],
    selectedDD2: "",
    ddOutput2: "",

  };


  selectStyle = {
    marginLeft: "0px",
    // position: "absolute",
    marginTop: "0px",
    width: "200px",
    height: "100px",
    fontSize: "20px"
  };

  promptStyle = {
    // marginLeft: "0px",
    // position: "absolute",
    marginTop: "20px",
    // width: "200px",
    // height: "100px",
    fontSize: "20px",
    fontWeight: "bold"
  };



  // ----------------------------------------------------------------------
  handleSelectChange = e => {
    var selectedDD_new = e.target.value;
    this.setState({ selectedDD: selectedDD_new });
  };

  handleSelectChange2 = e => {
    var selectedDD_new = e.target.value;
    this.setState({ selectedDD2: selectedDD_new });
  };

  handleSelectClick = () => {
    axios.get("http://127.0.0.1:5000/find_best_clutch_FT_Shooter/" + this.state.selectedDD + "/" + this.state.selectedDD2, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      alert('Result is : ' + response.data);
    }).catch(function (error) {
      alert(error);
    });
    var ddOutput_new = "User has selected Team <" + this.state.selectedDD + "> and Season <" + this.state.selectedDD2 + ">";
    this.setState({ ddOutput: ddOutput_new });

  };

  /* ===========================Render========================== */


  render() {
    return (
      <div style={{ paddingLeft: "100px" }} >

        {/* ============================Drop down select============================  */}
        <h5 style={this.promptStyle}>Find Best Clutch Free Throw Shooter:</h5>

        <div style={this.selectStyle}> {/*start drowm down div*/}

          <select onChange={this.handleSelectChange} >
            {
              this.state.dropDownList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>

          {/* <button
            onClick={this.handleSelectClick}
            className="btn btn-info btn-sm m-2"
          >
            SelectFind
        </button> */}


        </div > {/*end select div*/}

        {/* ============================Drop down select============================  */}
        <div style={{ marginTop: "-60px", width: "200px", height: "100px", fontSize: "20px" }}> {/*start drowm down div*/}

          <select onChange={this.handleSelectChange2} >
            {
              this.state.dropDownList2.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>
          <br></br>
          <button
            onClick={this.handleSelectClick}
            className="btn btn-info btn-sm m-2"
          >
            SelectFind
        </button>
          <p style={{ fontSize: "20px", width: "1000px" }}>{this.state.ddOutput}</p>

        </div > {/*end select div*/}



      </div >

    );

  }


}

export default Clutch;
