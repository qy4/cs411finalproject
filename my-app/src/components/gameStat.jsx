import React, { Component } from "react";
import axios from 'axios';
class GameStat extends Component {
  state = {
    inputedPlayer: "",
    playerOutput: "",

    dropDownMonthList: ["Unselected",'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    selectedMonth: "",
    monthOutput: "",

    dropDownTeamList: ["Unselected",'ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DAL', 'DEN', 'DET', 'GSW', 'HOU', 'IND', 'LAC', 'LAL', 'MEM', 'MIA', 'MIL', 'MIN', 'NOP', 'NYK', 'OKC', 'ORL', 'PHI', 'PHX', 'POR', 'SAC', 'SAS', 'TOR', 'UTA', 'WAS'],
    selectedTeam: "",
    teamOutput: "",

    dropDownSeasonList: ["Unselected","2018-19", "2017-18", "2016-17", "2015-16","2014-15"],
    selectedSeason: "",
    seasonOutput: "",

  };


  selectStyle = {
    marginLeft: "0px",
    // position: "absolute",
    marginTop: "0px",
    width: "200px",
    height: "100px",
    fontSize: "20px"
  };

  promptStyle = {
    // marginLeft: "0px",
    // position: "absolute",
    marginTop: "50px",
    // width: "200px",
    // height: "100px",
    fontSize: "20px",
    fontWeight: "bold"
  };



  // -------------------------Find ---------------------------------------------

  handleFindChange = e => {
    var inputedPlayer_new = e.target.value;
    this.setState({ inputedPlayer: inputedPlayer_new });
  };


  // handleFindClick = () => {

  //   // console.log("http://127.0.0.1:5000/players/" + this.state.btnFindInfo.playerName)
  //   // axios.get("http://127.0.0.1:5000/players/" + this.state.btnFindInfo.playerName, {
  //   //   headers: {
  //   //     'Access-Control-Allow-Origin': '*',
  //   //   }
  //   // }).then(function (response) {

  //   //   alert('Result is : ' + response.data);
  //   // }).catch(function (error) {
  //   //   alert(error);
  //   // });
  //   var playerOutput_new = "We now find info of player: " + this.state.inputedPlayer;
  //   this.setState({ playerOutput: playerOutput_new });
  // };


  // ---------------------------Drop Down 1: Month-------------------------------------------

  handleSelectMonthChange = e => {
    var selectedMonth_new = e.target.value;
    this.setState({ selectedMonth: selectedMonth_new });
  };

  handleMonthClick = () => {
    axios.get("http://127.0.0.1:5000/query_stats_by_month/" + this.state.inputedPlayer+"/"+this.state.selectedMonth, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      // result = response.data;

      alert('Result is : ' + response.data);
      // this.handleFindClickHelper(result);
    })
    var monthOutput_new = "User has entered user name <" + this.state.inputedPlayer +"> and selected month <" + this.state.selectedMonth + ">";
    this.setState({ monthOutput: monthOutput_new });

  };


  // ---------------------------Drop Down 2: Opponent Team-------------------------------------------

  handleSelectTeamChange = e => {
    var selectedTeam_new = e.target.value;
    this.setState({ selectedTeam: selectedTeam_new });
  };

  handleTeamClick = () => {
    axios.get("http://127.0.0.1:5000/query_stats_by_opponent/" + this.state.inputedPlayer+"/"+this.state.selectedTeam, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      // result = response.data;

      alert('Result is : ' + response.data);
      // this.handleFindClickHelper(result);
    })
    var teamOutput_new = "User has entered user name <" + this.state.inputedPlayer +"> and selected opponent team <" + this.state.selectedTeam + ">";
    this.setState({ teamOutput: teamOutput_new });

  };


  // ---------------------------Drop Down 3: Season-------------------------------------------
  handleSelectSeasonChange = e => {
    var selectedSeason_new = e.target.value;
    this.setState({ selectedSeason: selectedSeason_new });
  };


  handleSeasonClick = () => {
    axios.get("http://127.0.0.1:5000/query_all_results_groupby_opponent/"+ this.state.inputedPlayer + "/" + this.state.selectedSeason, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }).then(function (response) {
      // result = response.data;

      alert('Result is : ' + response.data);
      // this.handleFindClickHelper(result);
    })
    var seasonOutput_new  = "User has entered user name <" + this.state.inputedPlayer +"> and selected season: " + this.state.selectedSeason;
    
    this.setState({ seasonOutput: seasonOutput_new });

  };

  /* ===========================Render========================== */


  render() {
    return (
      <div style={{ paddingLeft: "100px" }} >
        <div> {/*start form+btn  div*/}
          {/* ===========================Find Button==========================  */}
          <br></br>
          <form>
            <label style={this.promptStyle}>
              1. Enter player name: 
            
            <input
                type="text"
                name="name"
                onChange={this.handleFindChange}
                placeholder="type in full name"
              />
            </label>
          </form>
          {/* <button
            onClick={this.handleFindClick}
            className="btn btn-info btn-sm m-2"
          >
            PlayerFind
        </button> */}
          <p style={{ fontSize: "20px" }}>{this.state.playerOutput}</p>

        </div> {/*end form+btn div*/}



        {/* ============================Drop down 1: Month============================  */}
        <h5 style={this.promptStyle}>2a. Select Month:</h5>

        <div style={this.selectStyle}> {/*start drowm down div*/}

          <select onChange={this.handleSelectMonthChange} >
            {
              this.state.dropDownMonthList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>
          <br></br>
          <button
            onClick={this.handleMonthClick}
            className="btn btn-info btn-sm m-2"
          >
            MonthSelect
        </button>
          <p style={{ fontSize: "20px", width: "1000px" }}>{this.state.monthOutput}</p>

        </div > {/*end select div*/}


        {/* ============================Drop down 2: Opponent Team============================  */}
        <h5 style={this.promptStyle}>2b. Select Opponent Team:</h5>

        <div style={this.selectStyle}> {/*start drowm down div*/}

          <select onChange={this.handleSelectTeamChange} >
            {
              this.state.dropDownTeamList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>

          <button
            onClick={this.handleTeamClick}
            className="btn btn-info btn-sm m-2"
          >
            TeamSelect
  </button>
          <p style={{ fontSize: "20px", width: "1000px"}}>{this.state.teamOutput}</p>

        </div > {/*end select div*/}


        {/* ============================Drop down 3: Season============================  */}
        <h5 style={this.promptStyle}>2c. Select Season:</h5>

        <div style={this.selectStyle}> {/*start drowm down div*/}

          <select onChange={this.handleSelectSeasonChange} >
            {
              this.state.dropDownSeasonList.map(choice => {
                return (<option key={choice} value={choice}> {choice}</option>);
              })
            }
          </select>

          <button
            onClick={this.handleSeasonClick}
            className="btn btn-info btn-sm m-2"
          >
            SeasonFind
        </button>
          <p style={{ fontSize: "20px", width: "1000px" }}>{this.state.seasonOutput}</p>

        </div > {/*end select div*/}




      </div >

    );

  }


}

export default GameStat;
