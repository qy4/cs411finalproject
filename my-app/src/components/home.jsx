import React, { Component } from 'react';
import Players from "./players";
import Ratings from "./ratings";
import GameStat from "./gameStat";
import Clutch from "./clutch"
import Predictor from "./predictor"



import { Route, Link, NavLink, BrowserRouter as Router } from 'react-router-dom'


const dagger = require("./dagger.png");

class Home extends Component {
    state = {}

    navStyle = {
        // marginLeft: "600px",
        // position: "absolute",
        // top: "30px",
        width: "200px",
        height: "80px",
        fontSize: "24px"
    };

    homeBtnStyle = {
        borderRadius: "95px",
        border: "5px solid #f56642",
        width: "120px",
        height: "120px",
        fontSize: "34px",
        // backgroundColor: "cyan",

        // position: "relative",
        // paddingLeft: "400px"
    }

    render() {
        return (
            <div style={{ backgroundColor: "#DBEDFA", height: "1500px" }}> {/*main div*/}
                <h1 style={{
                    textAlign: 'center', fontWeight: 'bold',
                    fontSize: 78,
                    // marginTop: 0,
                    // width: 200,
                    // backgroundColor: 'yellow',
                }}>Dagger</h1>

                <a><img src={dagger} alt="dagger placeholder" style={{ width: "90px", height: "90px", position: "absolute", marginLeft: "1000px", marginTop: "-80px" }} /></a>

                < Router >


                    <NavLink to="/" activeStyle={{}} role="btn" >
                        <button
                            className="btn btn-info btn-sm m-2"
                            style={this.homeBtnStyle}
                        >
                            Home
                                </button>
                    </NavLink>

                    <NavLink to="/players">
                        <button
                            className="btn btn-info btn-sm m-2"
                            style={this.navStyle}
                        >
                            Players
                                </button>
                    </NavLink>

                    <NavLink to="/ratings">
                        <button
                            className="btn btn-info btn-sm m-2"
                            style={this.navStyle}
                        >
                            Rate Players
                                </button>
                    </NavLink>

                    <NavLink to="/clutch">
                        <button
                            className="btn btn-info btn-sm m-2"
                            style={this.navStyle}
                        >
                            Explore Clutch Stats
                                </button>
                    </NavLink>
                    <NavLink to="/gameStat">
                        <button
                            className="btn btn-info btn-sm m-2"
                            style={this.navStyle}
                        >
                            Game Stat Deep Dive
                                </button>
                    </NavLink>

                    <NavLink to="/predictor">
                        <button
                            className="btn btn-info btn-sm m-2"
                            style={this.navStyle}
                        >
                            Player Stat Predictor
                                </button>
                    </NavLink>

                    <div>
                        <Route exact path="/" render={
                            () => { return (<h1>Welcome Home!</h1>) }
                        } />
                        <Route exact path="/players" component={Players} />
                        <Route exact path="/ratings" component={Ratings} />
                        <Route exact path="/clutch" component={Clutch} />
                        <Route exact path="/gameStat" component={GameStat} />
                        <Route exact path="/predictor" component={Predictor} />

                    </div>
                </Router >


            </div >
        );
    }
}

export default Home;