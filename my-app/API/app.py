from flask import Flask, render_template, request
from flask_mysqldb import MySQL
from flask_cors import CORS
import pandas as pd
# import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import pymongo
# import sklearn
# from sklearn.externals import joblib
import calendar
from operator import itemgetter
# from sklearn.linear_model import LinearRegression
app = Flask(__name__)
CORS(app)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '' # password is deleted
app.config['MYSQL_DB'] = 'NBA_DB'

# @app.route('/', methods=['GET', 'POST'])
# def index():
#     return "Hello Nuclear Geeks"
mysql = MySQL(app)

@app.route('/')
def users():
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM Game_Stats''')
    rv = cur.fetchall()
    return str(rv)

@app.route('/players', methods=['GET'])
def find_all_player():
    cur = mysql.connection.cursor()
    formula = '''SELECT * FROM Player_Bio'''
    cur.execute(formula)
    rv = cur.fetchall()
    return str(rv)


@app.route('/players/<string:name>', methods=['GET'])
def find_player(name):
    cur = mysql.connection.cursor()
    # formula = '''SELECT * FROM Player_Bio WHERE PlayerName = %s'''
    # cur.execute(formula, [name])
    # formula_1 = "SELECT * FROM Player_Bio WHERE PlayerName = '%s' and Season = '%s'"
    # sqlformula = formula_1 % (name)
    # df = pd.read_sql(sqlformula, conn)
    # rv = cur.fetchall()
    # return str(rv)
    formula_1 = "SELECT * FROM Player_Bio WHERE PlayerName = '%s'"
    sqlformula = formula_1 % name
    # df = pd.read_sql(sqlformula, conn)
    df = pd.read_sql(sqlformula, mysql.connection)
    return df.to_string()

@app.route('/players/<string:name>/<string:season>', methods=['GET'])
def find_player_season(name, season):
    cur = mysql.connection.cursor()
    formula = '''SELECT * FROM Player_Bio WHERE PlayerName = %s and Season = %s'''
    cur.execute(formula, [name, season])
    rv = cur.fetchall()
    return str(rv)

@app.route('/add_comment/<string:playerName>/<string:userName>/<string:userComments>/<string:ratings>', methods=['POST'])
def insert_comments_update_rating(playerName, userName, userComments, ratings):
    cur = mysql.connection.cursor()

    cur.execute("SELECT MAX(ratingID) FROM User_ratings")
    my_result = cur.fetchall()
    if not my_result[0][0]:
        new_ID = 1
    else:
        new_ID = int(my_result[0][0]) + 1


    sqlFormula = "INSERT INTO User_ratings (ratings, PlayerName, UserName, UserComments, ratingID) VALUES (%s, %s, %s, %s, %s)"
    records = (ratings, playerName, userName, userComments, new_ID)
    cur.execute(sqlFormula, records)
    mysql.connection.commit()

    # update rating
    updateRating(playerName)

    return "Comments successfully added and ratings updated!"



@app.route('/delete_comment/<string:deleteID>', methods=['POST'])
def delete_comments_update_rating(deleteID):
    cur = mysql.connection.cursor()

    #get playerName before deleting
    formula_1 = "SELECT PlayerName FROM User_ratings WHERE ratingID = %s"
    cur.execute(formula_1, deleteID)
    my_result = cur.fetchall()
    playerName = my_result[0][0]

    #delete record
    sqlformula = "DELETE FROM User_ratings WHERE ratingID = %s"
    cur.execute(sqlformula, [deleteID])

    #update avgrating of the player in Player_Bio
    updateRating(playerName)

    return "Comments successfully deleted and ratings updated!"

# Update Player rating when insert or delete comments
def updateRating(playerName):
    cur = mysql.connection.cursor()
    formula_1 = "SELECT Avg(ratings) as AvgRating FROM User_ratings WHERE PlayerName = %s"
    cur.execute(formula_1, [playerName])
    my_result = cur.fetchall()
    new_avgRatings = 0
    if my_result[0][0]:
        new_avgRatings = float(my_result[0][0])
    my_cur = mysql.connection.cursor()
    formula_2 = "UPDATE Player_Bio SET AvgRating = %s WHERE PlayerName = %s"
    my_cur.execute(formula_2, [new_avgRatings, playerName])
    mysql.connection.commit()


@app.route('/query_stats_by_month/<string:PlayerName>/<string:Month>', methods=['GET'])
def query_stats_by_month(PlayerName, Month):
    # cur = mysql.connection.cursor()
    Months = {}
    Months["Jan"] = "01"
    Months["Feb"] = "02"
    Months["Mar"] = "03"
    Months["Apr"] = "04"
    Months["May"] = "05"
    Months["Jun"] = "06"
    Months["Jul"] = "07"
    Months["Aug"] = "08"
    Months["Sep"] = "09"
    Months["Oct"] = "10"
    Months["Nov"] = "11"
    Months["Dec"] = "12"

    # my_cursor = conn.cursor()
    my_month = Months[Month]
    formula_1 = "SELECT PlayerName, Avg(Points) as Points, Avg(Rebounds) as Rebounds, Avg(Assists) as Assists, Avg(Steals) as Steals, count(*) as Game_Played, Season FROM Game_Stats WHERE PlayerName = '%s' and CAST(Date AS CHAR) LIKE '____%s__' group by Season"
    sqlformula = formula_1 % (PlayerName, my_month)
    df = pd.read_sql(sqlformula, mysql.connection)
    # mysql.connection.close()
    return df.to_string()

@app.route('/query_stats_by_opponent/<string:PlayerName>/<string:Opponent>', methods=['GET'])
def query_stats_by_opponent(PlayerName, Opponent):
    # cur = mysql.connection.cursor()
    formula_1 = "SELECT PlayerName, OpponentTeam, Avg(Points) as Points, Avg(Rebounds) as Rebounds, Avg(Assists) as Assists, Avg(Steals) as Steals, count(*) as Game_Played, Season FROM Game_Stats WHERE PlayerName = '%s' and OpponentTeam = '%s' group by Season"
    sqlformula = formula_1 % (PlayerName, Opponent)
    df = pd.read_sql(sqlformula, mysql.connection)
    return df.to_string()
    # conn.close()

@app.route('/query_all_results_groupby_opponent/<string:PlayerName>/<string:season>', methods=['GET'])
def query_all_results_groupby_opponent(PlayerName, season):
    # cur = mysql.connection.cursor()
    formula_1 = "SELECT PlayerName, OpponentTeam, Avg(Points) as Points, Avg(Rebounds) as Rebounds, Avg(Assists) as Assists, Avg(Steals) as Steals, count(*) as Game_Played, Season FROM Game_Stats WHERE PlayerName = '%s' and Season = '%s' Group By OpponentTeam"
    sqlformula = formula_1 % (PlayerName,season)
    df = pd.read_sql(sqlformula, mysql.connection)
    return df.to_string()
    # conn.close()

@app.route('/find_best_clutch_FT_Shooter/<string:TeamName>/<string:SeasonType>', methods=['GET'])
def find_best_clutch_FT_Shooter(TeamName, SeasonType):
    my_cursor = mysql.connection.cursor()

    my_cursor.execute('''Create view Jointed_Table as
     select * from (select * from Player_Clutch_Stats where Season = '2018-19') as short_clutch
     natural join (select distinct PlayerName, TeamName from Player_Bio where Season = '2018-19') as short_bio;''')

    my_cursor.execute('''Create view BestFT_Shooters as
    Select distinct PlayerName, Free_Throw_P, J.TeamName, J.Minutes_Played, J.SeasonType from Jointed_Table J,
    (Select max(Free_Throw_P) as maxFT, TeamName, SeasonType from Jointed_Table where Minutes_Played > 10 group by TeamName, SeasonType) as bestFT
    where J.Free_Throw_P = bestFT.maxFT and J.TeamName = bestFT.TeamName and J.Minutes_Played > 10;''')

    formula_1 = "select PlayerName, Free_Throw_P, TeamName, Minutes_Played, SeasonType from BestFT_Shooters where TeamName = '%s' and SeasonType = '%s'"
    sqlformula = formula_1 % (TeamName, SeasonType)
    df = pd.read_sql(sqlformula, mysql.connection)
    my_cursor.execute("Drop view Jointed_Table;")
    my_cursor.execute("Drop view BestFT_Shooters;")
    return df.to_string()
    # conn.close()

@app.route('/find_team_3PTA_ranking/<string:TeamName>/<string:Season>', methods=['GET'])
def find_team_3PTA_ranking(TeamName, Season):

    nbaTeams = {}
    nbaTeams['Atlanta Hawks'] = 'ATL'
    nbaTeams['Brooklyn Nets'] = 'BKN'
    nbaTeams['Boston Celtics'] = 'BOS'
    nbaTeams['Charlotte Hornets'] = 'CHA'
    nbaTeams['Chicago Bulls'] = 'CHI'
    nbaTeams['Cleveland Cavaliers'] = 'CLE'
    nbaTeams['Dallas Mavericks'] = 'DAL'
    nbaTeams['Denver Nuggets'] = 'DEN'
    nbaTeams['Detroit Pistons'] = 'DET'
    nbaTeams['Golden State Warriors'] = 'GSW'
    nbaTeams['Houston Rockets'] = 'HOU'
    nbaTeams['Indiana Pacers'] = 'IND'
    nbaTeams['LA Clippers'] = 'LAC'
    nbaTeams['Los Angeles Lakers'] = 'LAL'
    nbaTeams['Memphis Grizzlies'] = 'MEM'
    nbaTeams['Miami Heat'] = 'MIA'
    nbaTeams['Milwaukee Bucks'] = 'MIL'
    nbaTeams['Minnesota Timberwolves'] = 'MIN'
    nbaTeams['New Orleans Pelicans'] = 'NOP'
    nbaTeams['New York Knicks'] = 'NYK'
    nbaTeams['Oklahoma City Thunder'] = 'OKC'
    nbaTeams['Orlando Magic'] = 'ORL'
    nbaTeams['Philadelphia 76ers'] = 'PHI'
    nbaTeams['Phoenix Suns'] = 'PHX'
    nbaTeams['Portland Trail Blazers'] = 'POR'
    nbaTeams['Sacramento Kings'] = 'SAC'
    nbaTeams['San Antonio Spurs'] = 'SAS'
    nbaTeams['Toronto Raptors'] = 'TOR'
    nbaTeams['Utah Jazz'] = 'UTA'
    nbaTeams['Washington Wizards'] = 'WAS'
    nbaTeams_2 = {v: k for k, v in nbaTeams.items()}

    my_cursor = mysql.connection.cursor()
    sqlformula1 = '''Create view Jointed_Table as
    select * from (select * from Shot_Selection where Season = '%s') as
    short_shot natural join (select PlayerName, TeamName from Player_Bio where season = '%s') as short_bio;''' % (Season,Season)

    my_cursor.execute(sqlformula1)

    my_cursor.execute('''Create view Team_Shooting as
    select TeamName, sum(3FGA) as Total_3pt_attempt, sum(2FGA) as Total_2pt_attempt from Jointed_Table group by TeamName;''')


    sqlformula = "SELECT Total_3pt_attempt FROM Team_Shooting where TeamName = %s"
    my_cursor.execute(sqlformula, [TeamName])
    my_result = my_cursor.fetchall()
    Three_ptA = int(my_result[0][0])

    sqlformula = '''select count(*)+1 from Team_Shooting
    where Total_3pt_attempt > %s;'''

    my_cursor.execute(sqlformula, [Three_ptA])
    my_result = my_cursor.fetchall()
    ranking = my_result[0][0]
    my_cursor.execute("drop view Jointed_Table;")
    my_cursor.execute("drop view Team_Shooting")
    return ("The team %s attempted %s three pointers during %s season, which ranked No.%d in the league." % (nbaTeams_2[TeamName], Three_ptA, Season, ranking))
    # return "sdf"

    # conn.close()

@app.route('/find_team_2PTA_ranking/<string:TeamName>/<string:Season>', methods=['GET'])
def find_team_2PTA_ranking(TeamName, Season):

    nbaTeams = {}
    nbaTeams['Atlanta Hawks'] = 'ATL'
    nbaTeams['Brooklyn Nets'] = 'BKN'
    nbaTeams['Boston Celtics'] = 'BOS'
    nbaTeams['Charlotte Hornets'] = 'CHA'
    nbaTeams['Chicago Bulls'] = 'CHI'
    nbaTeams['Cleveland Cavaliers'] = 'CLE'
    nbaTeams['Dallas Mavericks'] = 'DAL'
    nbaTeams['Denver Nuggets'] = 'DEN'
    nbaTeams['Detroit Pistons'] = 'DET'
    nbaTeams['Golden State Warriors'] = 'GSW'
    nbaTeams['Houston Rockets'] = 'HOU'
    nbaTeams['Indiana Pacers'] = 'IND'
    nbaTeams['LA Clippers'] = 'LAC'
    nbaTeams['Los Angeles Lakers'] = 'LAL'
    nbaTeams['Memphis Grizzlies'] = 'MEM'
    nbaTeams['Miami Heat'] = 'MIA'
    nbaTeams['Milwaukee Bucks'] = 'MIL'
    nbaTeams['Minnesota Timberwolves'] = 'MIN'
    nbaTeams['New Orleans Pelicans'] = 'NOP'
    nbaTeams['New York Knicks'] = 'NYK'
    nbaTeams['Oklahoma City Thunder'] = 'OKC'
    nbaTeams['Orlando Magic'] = 'ORL'
    nbaTeams['Philadelphia 76ers'] = 'PHI'
    nbaTeams['Phoenix Suns'] = 'PHX'
    nbaTeams['Portland Trail Blazers'] = 'POR'
    nbaTeams['Sacramento Kings'] = 'SAC'
    nbaTeams['San Antonio Spurs'] = 'SAS'
    nbaTeams['Toronto Raptors'] = 'TOR'
    nbaTeams['Utah Jazz'] = 'UTA'
    nbaTeams['Washington Wizards'] = 'WAS'
    nbaTeams_2 = {v: k for k, v in nbaTeams.items()}

    my_cursor = mysql.connection.cursor()
    sqlformula1 = '''Create view Jointed_Table as
    select * from (select * from Shot_Selection where Season = '%s') as
    short_shot natural join (select PlayerName, TeamName from Player_Bio where season = '%s') as short_bio;''' % (Season,Season)

    my_cursor.execute(sqlformula1)

    my_cursor.execute('''Create view Team_Shooting as
    select TeamName, sum(3FGA) as Total_3pt_attempt, sum(2FGA) as Total_2pt_attempt from Jointed_Table group by TeamName;''')


    sqlformula = "SELECT Total_2pt_attempt FROM Team_Shooting where TeamName = %s"
    my_cursor.execute(sqlformula, [TeamName])
    my_result = my_cursor.fetchall()
    Three_ptA = int(my_result[0][0])

    sqlformula = '''select count(*)+1 from Team_Shooting
    where Total_2pt_attempt > %s;'''

    my_cursor.execute(sqlformula, [Three_ptA])
    my_result = my_cursor.fetchall()
    ranking = my_result[0][0]
    my_cursor.execute("drop view Jointed_Table;")
    my_cursor.execute("drop view Team_Shooting")
    return ("The team %s attempted %s two pointers during %s season, which ranked No.%d in the league." % (nbaTeams_2[TeamName], Three_ptA, Season, ranking))
    # return "sdf"

@app.route('/find_best_clutch_3Pointer_Shooter/<string:TeamName>/<string:SeasonType>', methods=['GET'])
def find_best_clutch_3Pointer_Shooter(TeamName, SeasonType):
    my_cursor = mysql.connection.cursor()

    my_cursor.execute('''Create view Jointed_Table as
     select * from (select * from Player_Clutch_Stats where Season = '2018-19') as short_clutch
     natural join (select distinct PlayerName, TeamName from Player_Bio where Season = '2018-19') as short_bio;''')

    my_cursor.execute('''Create view BestFT_Shooters as
    Select distinct PlayerName, 3pointer_P, J.TeamName, J.Minutes_Played, J.SeasonType from Jointed_Table J,
    (Select max(3pointer_P) as maxFT, TeamName, SeasonType from Jointed_Table where Minutes_Played > 10 group by TeamName, SeasonType) as bestFT
    where J.3pointer_P = bestFT.maxFT and J.TeamName = bestFT.TeamName and J.Minutes_Played > 10;''')

    formula_1 = "select PlayerName, 3pointer_P, TeamName, Minutes_Played, SeasonType from BestFT_Shooters where TeamName = '%s' and SeasonType = '%s'"
    sqlformula = formula_1 % (TeamName, SeasonType)
    df = pd.read_sql(sqlformula, mysql.connection)
    my_cursor.execute("Drop view Jointed_Table;")
    my_cursor.execute("Drop view BestFT_Shooters;")
    return df.to_string()

# Advanced Function 1
@app.route('/stat_predictor/<string:Player_Name>/<string:Opponent_Team>/<string:Month>', methods=['GET'])
def stat_predictor(Player_Name, Opponent_Team, Month):
    Months = {}
    Months["Jan"] = 1
    Months["Feb"] = 2
    Months["Mar"] = 3
    Months["Apr"] = 4
    Months["May"] = 5
    Months["Jun"] = 6
    Months["Jul"] = 7
    Months["Aug"] = 8
    Months["Sep"] = 9
    Months["Oct"] = 10
    Months["Nov"] = 11
    Months["Dec"] = 12
    # cur = mysql.connection.cursor()
    # formula = '''call I_Hate_Your_Team(%s, %s)'''
    # cur.execute(formula, [Player_Name, Opponent_Team])
    # df = cur.fetchall()
    sqlformula = 'call I_Hate_Your_Team("%s", "%s")' % (Player_Name, Opponent_Team)
    df = pd.read_sql(sqlformula, mysql.connection)
    # print(df)
    # return str(rv)

    Month_dict = dict((v,k) for k,v in enumerate(calendar.month_abbr))
    Month_digit = [Month_dict[key] for key in df.Month.tolist()]
    df.Month = Month_digit
    df = df.sort_values(by=['Month'])

    X = [[value] for value in df.Month.tolist()]
    point = [value for value in df.AVG_Points.tolist()]
    rebound = [value for value in df.AVG_Rebounds.tolist()]
    assist = [value for value in df.AVG_Assists.tolist()]
    steals = [value for value in df.AVG_Steals.tolist()]

    poly_reg = PolynomialFeatures(degree=2)
    X_poly = poly_reg.fit_transform(X)
    pol_reg_point = LinearRegression()
    pol_reg_point.fit(X_poly, point)
    # pol_reg_rebound = sklearn.linear_model.LinearRegression()
    pol_reg_rebound = LinearRegression()
    pol_reg_rebound.fit(X_poly, rebound)
    pol_reg_assist = LinearRegression()
    pol_reg_assist.fit(X_poly, assist)
    pol_reg_steal = LinearRegression()
    pol_reg_steal.fit(X_poly, steals)

    plt.clf()
    plt.plot(X, pol_reg_point.predict(poly_reg.fit_transform(X)), color='blue',label = "Points")
    plt.plot(X, pol_reg_rebound.predict(poly_reg.fit_transform(X)), color='red',label = "Rebounds")
    plt.plot(X, pol_reg_assist.predict(poly_reg.fit_transform(X)), color='green',label = "Assists")
    plt.plot(X, pol_reg_steal.predict(poly_reg.fit_transform(X)), color='yellow',label = "Steals")
    plt.title('Stat_Prediction: %s vs %s' %(Player_Name, Opponent_Team))
    plt.xlabel('Month')
    plt.ylabel('Stats')
    plt.legend(loc="upper right")
    s = Player_Name[0:3]
    # plt.savefig('../src/components/advanced_one'+s+Opponent_Team+Month+'.png')
    plt.savefig('advanced_one.png')

    predicted_point = pol_reg_point.predict(poly_reg.fit_transform([[Months[Month]]]))
    predicted_rebound = pol_reg_rebound.predict(poly_reg.fit_transform([[Months[Month]]]))
    predicted_assist = pol_reg_assist.predict(poly_reg.fit_transform([[Months[Month]]]))
    predicted_steal = pol_reg_steal.predict(poly_reg.fit_transform([[Months[Month]]]))

    predicted_stat_line = "%s will have %d points, %d rebounds, %d assists, %d steals against %s." %(Player_Name, int(predicted_point[0]), int(predicted_rebound[0]), int(predicted_assist[0]), int(predicted_steal[0]),Opponent_Team)
    return predicted_stat_line

# Advanced Function 2
@app.route('/ten_best_teammates/<string:PlayerName>', methods=['GET'])
def ten_best_teammates(PlayerName):
    # This line is deleted for password
    db = client.get_database('NBAMongo_db')
    records = db.Team_Roaster
    a  = records.aggregate([{'$unwind':"$Players"},{'$match':{'Players': PlayerName}},{'$project':{'_id':0, 'name': 1, 'Season': 1}},{'$sort':{'Season': 1}}])
    result = list(a)
    b = records.aggregate([{'$unwind':"$Players"},{'$project':{'_id':0, 'name': 1, 'Players':1, 'Season': 1}}])
    individual_roaster = list(b)

    teammates = {}
    for row_1 in result:
        for row_2 in individual_roaster:
            if row_2['name'] == row_1['name'] and row_2['Season'] == row_1['Season'] and row_2['Players'] != PlayerName:
                key = row_2['Players']
                if key in teammates:
                    teammates[key] += 1
                else:
                    teammates[key] = 1

    final_result = sorted(teammates.items(), key=itemgetter(1),reverse=True)[0:10]


    Two_point_Attempt = []
    Two_point_percentage = []
    Three_point_Attempt = []
    Three_point_percentage = []
    Seasons = []

    for row in result:
        if(row['Season'] == '2019-20'):
            continue
        sqlformula = "Select 2FGA, 2FG_P, 3FGA, 3FG_P from Shot_Selection where Season = '%s' and Team = '%s' and PlayerName = '%s'"
        final_command = sqlformula % (row['Season'],row['name'],PlayerName)
        # my_cursor = conn.cursor()
        my_cursor = mysql.connection.cursor()
        my_cursor.execute(final_command)
        my_result = my_cursor.fetchall()
        Two_point_Attempt.append(my_result[0][0])
        Two_point_percentage.append(my_result[0][1]/100)
        Three_point_Attempt.append(my_result[0][2])
        Three_point_percentage.append(my_result[0][3]/100)
        Seasons.append(row['Season'])
    plt.clf()
    plt.plot(Seasons,Two_point_Attempt,label = '2PTA')
    plt.plot(Seasons,Three_point_Attempt, label = '3PTA')
    plt.legend(loc="upper right")
    plt.title("Shot Selection Change of %s" % PlayerName)
    plt.savefig('advanced_two_pg1.png')
    plt.plot(Seasons,Two_point_percentage,label = '2PT_P')
    plt.plot(Seasons,Three_point_percentage, label = '3PT_P')
    plt.title("Shot Percentage Change of %s" % PlayerName)
    plt.legend(loc="upper right")
    plt.savefig('advanced_two_pg2.png')
    return str(final_result)


if __name__ == '__main__':
    app.run()
